//
//  BbbView.swift
//  MVP_Template
//
//  Created by Johnson Hsu on 2018/5/2.
//  Copyright © 2018年 Johnson Hsu. All rights reserved.
//

import Foundation

// MARK: View
protocol BbbView {
    
    func initPresenter()
    
    func getWebsiteCategory() -> [UserProfile.WebsiteCategory]
    
}


// MARK: Presenter
public class BbbPresenter {
    
    var view: BbbView
    
    init(_ view: BbbView) {
        self.view = view
    }
    
    func updateWebsiteCategory() {
        
    }
    
}

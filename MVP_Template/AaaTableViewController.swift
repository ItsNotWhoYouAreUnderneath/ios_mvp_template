//
//  AaaTableViewController.swift
//  MVP_Template
//
//  Created by Johnson Hsu on 2018/4/25.
//  Copyright © 2018年 Johnson Hsu. All rights reserved.
//

import UIKit

class AaaTableViewController: UITableViewController, AaaView {

    var tableEnabled: Bool = true
    
    @IBAction func disableTableView(_ sender: UIButton) {
        if tableEnabled {
            tableView.allowsSelection = false
            tableEnabled = false
        } else {
            tableView.allowsSelection = true
            tableEnabled = true
        }
        
        
//        for c in tableView.visibleCells {
//
//        }
    }
    var presenter: AaaPresenter?
    
    var userProfile: UserProfile?
    
    var ageClassifications: [AgeClassification] = [
        AgeClassification(id: 1, name: "國小前：7歲以下", limits: [], check: false),
        AgeClassification(id: 2, name: "國小：7-12歲", limits: [], check: false),
        AgeClassification(id: 3, name: "國中：13-15歲", limits: [], check: false),
        AgeClassification(id: 4, name: "高中：16-18歲", limits: [], check: false),
        AgeClassification(id: 5, name: "成人：18+", limits: [], check: false),
        AgeClassification(id: 6, name: "客製設定", limits: [], check: false)
    ]
    
    var selectedAge: AgeClassification?
    
    func initPresenter() {
        presenter = AaaPresenter(self)
    }

    @IBAction func aaaButton(_ sender: UIButton) {
        performSegue(withIdentifier: "AaaToBbb", sender: ageClassifications[0])
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        initPresenter()
//        selectAgeFromUserProfile()
        selectedAge = ageClassifications[1]
        
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return ageClassifications.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "AaaCell", for: indexPath)

        cell.textLabel?.text = ageClassifications[indexPath.row].name
        
        if ageClassifications[indexPath.row].check! {
            cell.textLabel?.text = "V" + (cell.textLabel?.text)!
        }

        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        var cell = tableView.cellForRow(at: indexPath)
        tableView.visibleCells.forEach { (c) in
            if c == cell {
                if c.accessoryType == .checkmark {
                    // did not change
                } else {
                    c.accessoryType = .checkmark
                    // alart update...
                    
                }
            } else {
                c.accessoryType = .none
            }
        }
        
    }
 
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "AaaToBbb" {
            if let indexPath = self.tableView.indexPathForSelectedRow {
                let controller = segue.destination as! BbbTableViewController
                ageClassifications[indexPath.row].check = true
                selectedAge = ageClassifications[indexPath.row]
                controller.selectedAge = selectedAge
            }
        }
    }

    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

    /**
        To compare UserProfile.blockedCategories with categoryDict to figure out which age classification
        was setted.
     
        
     */
    func selectAgeFromUserProfile() {
        var blocks: [UserProfile.WebsiteCategory] = (userProfile?.blockedCategories)!
        
        // follow the order does matter
        let categories: [CategoryCode] = [.CAT1, .CAT2, .CAT3, .CAT4, .CAT5, .CAT6]
        var match = false
        var cat: CategoryCode?
        for c in categories {
            cat = c
            for wc in categoryDict[c]! {
                // check if a WebsiteCategoryItem is in the block list
                match = false
                for (i, b) in blocks.enumerated() {
                    if b == wc {
                        blocks.remove(at: i)
                        match = true
                        break
                    }
                }
                if !match {
                    break;
                }
            }
            if blocks.isEmpty {
                break;
            }
        }
        
        if !match {
            selectedAge = ageClassifications[5]
        } else {
            switch cat! {
            case .CAT5:
                selectedAge = ageClassifications[0]
            case .CAT4:
                selectedAge = ageClassifications[1]
            case .CAT3:
                selectedAge = ageClassifications[2]
            case .CAT2:
                selectedAge = ageClassifications[3]
            case .CAT1:
                selectedAge = ageClassifications[4]
            case .CAT6:
                selectedAge = ageClassifications[5]
            }
        }
    }
    
}

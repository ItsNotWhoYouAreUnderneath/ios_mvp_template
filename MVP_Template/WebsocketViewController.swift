//
//  WebsocketViewController.swift
//  MVP_Template
//
//  Created by Johnson Hsu on 2018/5/13.
//  Copyright © 2018年 Johnson Hsu. All rights reserved.
//

import UIKit
import Starscream
import SystemConfiguration.CaptiveNetwork
//import NetworkExtension
import CryptoSwift




class WebsocketViewController: UIViewController, WebSocketDelegate {
    
    var socket: WebSocket?
    
    @IBOutlet weak var wsurl: UITextField!
    @IBOutlet weak var message: UITextField!
    @IBOutlet weak var echo: UITextView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    @IBAction func connect(_ sender: UIButton) {
        wsurl.text = "ws://localhost:8080/examples/websocket/echoAnnotation"
//        guard let url = wsurl.text else {
//            return
//        }
//
//        socket = WebSocket(url: URL(string: url)!)
//        socket!.delegate = self
//        socket!.connect()
        
//        getInterfaces()
//        openSetup()
//        list()
//        scan()
        
//        encrypt()
        decrypt()
        
    }
    
    @IBAction func send(_ sender: UIButton) {
        guard let socket = socket, let text = message.text  else {
            return
        }
        
        socket.write(string: text)
        message.text = ""
        self.setEditing(false, animated: true)
    }
    
    func websocketDidConnect(socket: WebSocketClient) {
        print("websocket is connected")
    }
    
    func websocketDidDisconnect(socket: WebSocketClient, error: Error?) {
        print("websocket is disconnected: \(error?.localizedDescription)")
    }
    
    func websocketDidReceiveMessage(socket: WebSocketClient, text: String) {
        print("got some text: \(text)")
        echo.text.write(text + "\n")
    }
    
    func websocketDidReceiveData(socket: WebSocketClient, data: Data) {
        print("Got pong! Maybe some data: \(data.count)")
    }
    
    func getInterfaces() -> Bool {
        guard let unwrappedCFArrayInterfaces = CNCopySupportedInterfaces() else {
            print("this must be a simulator, no interfaces found")
            return false
        }
        guard let swiftInterfaces = (unwrappedCFArrayInterfaces as NSArray) as? [String] else {
            print("System error: did not come back as array of Strings")
            return false
        }
        for interface in swiftInterfaces {
            print("Looking up SSID info for \(interface)") // en0
            guard let unwrappedCFDictionaryForInterface = CNCopyCurrentNetworkInfo(interface as CFString) else {
                print("System error: \(interface) has no information")
                return false
            }
            guard let SSIDDict = (unwrappedCFDictionaryForInterface as NSDictionary) as? [String: AnyObject] else {
                print("System error: interface information is not a string-keyed dictionary")
                return false
            }
//            for d in SSIDDict.keys {
//                print("\(d): \(SSIDDict[d]!)")
//            }
            print("ssid:\(SSIDDict[kCNNetworkInfoKeySSID as String]!)")
        }
        return true
    }
    
    func openSetup() {
        let url = URL(string: "App-Prefs:root=WIFI") //for WIFI setting app
        let app = UIApplication.shared
        app.openURL(url!)
    }
    
//    func scan() {
//        // Johnson's AirPort Time Capsule
//        var hotspotConfig = NEHotspotConfiguration.init(ssid: "Family+88:6A:E3:FF:D3:3E")
//        print(hotspotConfig)
////        var helper = NEHotspotHelper.init()
//
//        NEHotspotConfigurationManager.shared.apply(hotspotConfig) {[unowned self] (error) in
//            if let error = error {
//                print(error)
//            } else {
//                print("success")
//            }
//        }
//
//    }
//
//    func list() {
//        NEHotspotConfigurationManager.shared.getConfiguredSSIDs() { ssid in
//            print(ssid)
//        }
//    }
    
    func encrypt() {
//        let key: Array<UInt8> = Data(base64Encoded: "aHQrVHRkbkdEVS92WnVBSmFJS3d4TEY2UTZJPYvUq4s=", options: Data.Base64DecodingOptions(rawValue: 0))!.bytes
//        let key: Array<UInt8> = Data(base64Encoded: "GkoX2xxpoHyKSPDnAUd8vqxymvYv6M4TUe1MNxGPI5s=", options: Data.Base64DecodingOptions(rawValue: 0))!.bytes
        
        let key: Array<UInt8> = Data(base64Encoded: "aDBaN0djVXhBcVBTMWE2eGhkdUt1dHVoRWJvPc+lyLg=", options: Data.Base64DecodingOptions(rawValue: 0))!.bytes
        
//        let input = """
//{"hello":"john"}
//"""
        
        //{"messageType":"WifiRouterConfig",
        let input = """
{"wlansPassPhrase":"12345678", "wlansType":"WPA+2PSK", "wlansEnabled":"true", "wlansEncryption":"TKIP", "cfgVersion":4835002}
"""
        
        do {
            let aesDes: AES = try AES(key: key, blockMode: .ECB, padding: .pkcs7)
            let r = try aesDes.encrypt(input.bytes)
            print(r.toHexString())
        } catch {
            print(error)
        }
    }
    
    func decrypt() {
//        let key: Array<UInt8> = Data(base64Encoded: "aHQrVHRkbkdEVS92WnVBSmFJS3d4TEY2UTZJPYvUq4s=", options: Data.Base64DecodingOptions(rawValue: 0))!.bytes
//        let key: Array<UInt8> = Data(base64Encoded: "GkoX2xxpoHyKSPDnAUd8vqxymvYv6M4TUe1MNxGPI5s=", options: Data.Base64DecodingOptions(rawValue: 0))!.bytes
        
        let key: Array<UInt8> = Data(base64Encoded: "aDBaN0djVXhBcVBTMWE2eGhkdUt1dHVoRWJvPc+lyLg=", options: Data.Base64DecodingOptions(rawValue: 0))!.bytes
        
//        let input = "c95fc8b7437f2c4b508019ecfd38764706edac998011127c3b12886b412de0a06fcce264bef8dfef3c267a980496ead5"
//        let input = "80df783217483df6332b48e3f0999b4eb6e80b04a9a943881586abe15e4d6aa7"
//        let input = "124693a428ee434125158bc98ffe85742afe66b7243040d8dd19e3f3b50dd328ef129e9a71c0c2ffcc49d4619189c9fe"
//        let input = "c15e8be60fc9910b7b2762b36f8e99246d2db4be924c1503e505a3a4327e090c3bb19046fdae8203c89df19708bd39a93512844666f2ab60195f3c127b73720acaa6ae2fc717a254be3571d9276d29184ea54e89305f67355c1056441fbc5b9bb3d544a6599be9b2de2e12949355d9e5169a48871241d1fa7e695f72699173a95f7baf8769cfc0958e589ab7b5b309eafbdbf98f67d025adea343fbc64b8c0363352db43f0657c67950169596af20aa4cb7f6c7adbfa2642b5f462f680a111e41a74bc63bad3bb634b586c1e8bb3820b8c619e8afbce753dcce6241c863ceaa315deabd03b4ad1a5e1f48917eb008a7ede6d947e9b22181c2170cb5fb74f7ca7bc85325c12381ff7ea66f5b2e3e687fa5f0a9c273bac4c35b44a0279e6afb6b8"
        
        // GetRouterStatus
//        let input = """
//5ad132ec4682d3f852887616b13ee19295292e084c00b917d0bb53a8bef644c65f171bf785bd4f4d0362be9b941c9c4f11802dba298fcb3ecca4ab2f1743c859b90c77de92d64acfead0aee3db43022852f02071afbb51e6d2017b91c5cad349ec2f266370c71d1270d877c589324cb5a70b6c39a2e484b80a0cb56d7dd85f92951d64917149beee71b00ad5753ada6d3b9ea2a3564a53e2e747fad83c26365176e822c8d5100b9852f8f45f0632660284008c21bcd7584b043afe9436c80437492475956f509af09133527bded458880eb8d151d6af8d356de574daa5cb79f9c330c51f45d0c11451c1e4f14d145f07ebd937cbc4a3217315554247618c4db8477f7a4187d65855ec21b8acdd0900837ee49dc37bdeb89df7fa86f290142afe
//"""
        
        // GetRouterConfig
//        let input = """
//5ad132ec4682d3f852887616b13ee19295292e084c00b917d0bb53a8bef644c65f171bf785bd4f4d0362be9b941c9c4f11802dba298fcb3ecca4ab2f1743c859c32411deb502acadd2214a06ca18444d28c3556cbe0349e88a0851809ef12759a106d6d2a9e30fe9728a2b416b375e79bd8a8254a19fee3a9aa3fe439092e848017e19550b0ab3c79031eb20a5a027578f44f9edad7eefd154176a9ec909802bec6faafff55ef403bfe275a3d7dd1e05961adf0e1f3d660f18465076fc7ecac96d4135010f15eca21e468291156893b7f7cb1413c16306815fa268d05ae24e97425fe942463bbb548f29dbd5d67ae881a5a9cd835f9b0eace6010d0b4ceaf6f7da84cc67115c29baa5ef345a3a6c607157e0f3d820776131e87134bf706b2dd83ae777bb88bac7c0e2d154a7a1d0b75e6d33345137291a74f0ff35b6691e52d5ffdfe6150ce87e19b09b998fe67253022fdeade216675617ee1fe1b7045de9645e0bcd08cd66918420674e7cbb8c8425a41e405775d3a86f6c30e49504170a9b0eea55fb6bd80e095f505cc2c54e6ef93637673352877ea49275e07177fcac29ac27bbd69daf5d99fb828f4a5b11c805feeee9322284cce7f94951e53b25887b3f67df4fb8bae1736bc37b18c0048f57233ae43e334d479aebe09f671ab218b12b24ccef74b385666ba391832b657c69b6b43fd4cdac1b824106d4c0ab905e0b6fa7452026e582de659b66ead29b4f55ad3359950b7f3947bb6430309e261c655ee0333ad70b589c92cb9c525d2919bad604924777b750d58438ec03cfa84d6f498a1e7e07b139caaa3f271bc691ae4e0c5c4379605b6c466938a8c59c2962025e754a24366ca7a3d948cdc428034bfdd6519306cea03efbc573effecf028bc1c413df6be0246a2e01cd2f73a26ce5977f8ab3061f2d555cb1ea3a2da37a4e8f4ae1621a4c795d894a77bef9d5c6b269
//"""
        
        // GetFpAgentId
//        let input = """
//5ad132ec4682d3f852887616b13ee19295292e084c00b917d0bb53a8bef644c65f171bf785bd4f4d0362be9b941c9c4f1122abc6891b2d5703d4f4feab2b4a43c1a7f42edab40c27a4d6a4b1cdafbbacc9c3b52bbc168c775aa04f5f1bf48e37358e798f45ed57015eaa7f35ec08fccc7ac705868352a963d2e64aac243e912e
//"""

        // GetInternetStatus
//        let input = """
//5ad132ec4682d3f852887616b13ee19295292e084c00b917d0bb53a8bef644c65f171bf785bd4f4d0362be9b941c9c4fa2f0525a3725bc31e8a0bed41d83b1eefad5dd62d20adb9c335a9e1c4a85a0c28a8d0d78173a39a2006e43fd35a7b01da250e1bdf931d5075b76ff98d12cb14bf8c4c71a91556c47d3b6ba71345fdf47bcd37f9c04c459ace1e302437535df568c57619bf7bcb392f813dc1376fdf61492bbd7d7f141ea9d111704508fc5b3eac4e238d981cb38e14c78f115dfc740655680cbb83e9d10f36b2d31f0de0390ac
//"""
        
//        let input = "c95fc8b7437f2c4b508019ecfd38764706edac998011127c3b12886b412de0a06fcce264bef8dfef3c267a980496ead5"
        
        // response of /SetWifiRouterConfig to change password
//        let input = "84a6f93315c27bae568a970c1b42705524d0971c06681b9da41a4fa78cfd7328"
        
//        let input = """
//5ad132ec4682d3f852887616b13ee19295292e084c00b917d0bb53a8bef644c65f171bf785bd4f4d0362be9b941c9c4f11802dba298fcb3ecca4ab2f1743c859b90c77de92d64acfead0aee3db43022852f02071afbb51e6d2017b91c5cad349ec2f266370c71d1270d877c589324cb5a70b6c39a2e484b80a0cb56d7dd85f92259bea0a077e4ac2ef9e3f42667024275c845c4ddd0917696b27adf37752f86d14688f459f83730ab2d335326641e57a90f4e86281851e236f236bc288810c503e8d307f192b23fde08400f1780c70e4ed74edb683908081d63ce6cba06afb7754213ecf1d90d82adb9417d83c44d0053e8fea555b9e2ce8a0da8e177a58951f0743cf9caa16886493c5538d03245c9b165592433f8a09c568dd7d9b605d0b4e07273533d908e72a84f0e8a662e2e1d0d37460dff9fc6eae4cff1cd708295c51
//"""
        /*  decode from upper string
 {"errorCode": 0, "message": "Success", "result": {"propSet": "wifiRtStatus", "internetStatus": "false", "firmwareVersion": "1.3.10", "agentVersion": "1.3.10d", "agentId": "ht+TtdnGDU/vZuAJaIKwxLF6Q6I=", "webfilterIp": "", "webfilterStatus": "false", "serialNo": "88:6a:e3:ff:d3:3e", "cfgVersion": 4835000}}
 */
        
//        let input = """
//d8b0dca2e91f384ebc86521056c0edbae9c0d17957b3ccfea1fe0d7c3a4469bb5e81a889bd680d9b09e803ca029cb48c1cc0a8fc4322f3c56c51f47f57a7206b13f991c7320821b0bdcbf4aa2b8b78ced7eb16c5a072505bf6a7bdbd258ea39cc10dc8c65b62476b1e9910cac4418fdf8a87ecdf2df487b1012af003b23473a7152b49bdaf029097073725e6d1beb29b2271839405768a4617c5b2896b92c552b975f091c331357efa6053de3309bd07d37460dff9fc6eae4cff1cd708295c51
//"""
        
//        let input = """
//5ad132ec4682d3f852887616b13ee19295292e084c00b917d0bb53a8bef644c65f171bf785bd4f4d0362be9b941c9c4fd452e5b84590b4f314aff10ac23ff7a1b0f6ce88abf882477dc8182a484149f5f5f54114bfd05f2807105a4067b99f3207c8f99330d5a7e0d4b7688d92f3f60952821e0f550a9c8e79973307735c978d2cb5786fa91a635c400797378da059e2c5059c36a5a0115e6a3b750b0c930927d083728e950ec0e02f4ae697eb3a1bba80cecc10787a0b7ebfa1fcf1f9621d63
//"""

//        let input = """
//de158d8630d20583866ca2739d07b01872301645bb66ad5c07c0aa78f6e6dd82355c8106e6c2b7d3a79a59e75605f6e199f0cbe8ef90f558e0e56ef0dd53a13dc18b69d08dce7821b124221059966b2fe58716affdc2aaad5805191ba0c29c2f7589435f899d8258f683d36c3ebb4cadb2b094c655a263bd72f03e33ce30b19c11d2a878de539064ff3cc8e857bd7f1dbd12f47b7799cc9b579154d261fea3dbaaa9be50149e5556b4e46322357046cd5ad6bfc71b6b86fd54b27eb2d8900b52712ea5fbfb0c0cd192b52d2e70aa083559fe38a6aa9f1f3ac14e4d068e5b14ea22ba260330bf006524b97762e113db0a0c8790ad8dbda0a37ccd3fdb98de5493bae4676c6deb4da6b78a1a406f76ace54d4dd05e190a5f2bde3140319409160e7d8e805a1a80fca4c52f82ae099f0e540ca569c74295e9bacb5c901797ddac20
//"""
        
//        let input = """
//de158d8630d20583866ca2739d07b01872301645bb66ad5c07c0aa78f6e6dd82355c8106e6c2b7d3a79a59e75605f6e17116c83f289e9f9efff0bbbc79765a0450b47d1339865c498bb4619dbfc14d8ecace77814aa3abcc44ae1059f390236d8f450c84f4d475f4027f247b9e7979741acb8929c52a67541cfe756fb045eabf0fa4de740b804e066b06368404bc689e9b94a7ad69af7af2aebffc9dbb2d906f15730ec1ad057c49db0d68dd672f0fd62d69fcef4e4f4b4bd7d330b28b151851
//"""
        
//        let input = """
//de158d8630d20583866ca2739d07b01872301645bb66ad5c07c0aa78f6e6dd82355c8106e6c2b7d3a79a59e75605f6e199f0cbe8ef90f558e0e56ef0dd53a13dc18b69d08dce7821b124221059966b2fe58716affdc2aaad5805191ba0c29c2f7589435f899d8258f683d36c3ebb4cadb2b094c655a263bd72f03e33ce30b19c11d2a878de539064ff3cc8e857bd7f1dbd12f47b7799cc9b579154d261fea3dbaaa9be50149e5556b4e46322357046cd5ad6bfc71b6b86fd54b27eb2d8900b52712ea5fbfb0c0cd192b52d2e70aa083559fe38a6aa9f1f3ac14e4d068e5b14ea22ba260330bf006524b97762e113db0a0c8790ad8dbda0a37ccd3fdb98de5493bae4676c6deb4da6b78a1a406f76ace54d4dd05e190a5f2bde3140319409160e7d8e805a1a80fca4c52f82ae099f0e54ca64208a3aeeefef47a490ef9655f04
//"""
        
//        let input = """
//de158d8630d20583866ca2739d07b01872301645bb66ad5c07c0aa78f6e6dd82355c8106e6c2b7d3a79a59e75605f6e17116c83f289e9f9efff0bbbc79765a04d9be147c9eb2d8f9dad22434d67e3437cace77814aa3abcc44ae1059f390236d8f450c84f4d475f4027f247b9e7979741acb8929c52a67541cfe756fb045eabf0fa4de740b804e066b06368404bc689e9b94a7ad69af7af2aebffc9dbb2d906f15730ec1ad057c49db0d68dd672f0fd6a06788e94d4367920af8e892b143a02b
//"""

        let input = """
5ad132ec4682d3f852887616b13ee19295292e084c00b917d0bb53a8bef644c65f171bf785bd4f4d0362be9b941c9c4f11802dba298fcb3ecca4ab2f1743c859b90c77de92d64acfead0aee3db43022852f02071afbb51e6d2017b91c5cad349ec2f266370c71d1270d877c589324cb5a70b6c39a2e484b80a0cb56d7dd85f92259bea0a077e4ac2ef9e3f42667024275c845c4ddd0917696b27adf37752f86d14688f459f83730ab2d335326641e57a90f4e86281851e236f236bc288810c503e8d307f192b23fde08400f1780c70e4ed74edb683908081d63ce6cba06afb7754213ecf1d90d82adb9417d83c44d0053e8fea555b9e2ce8a0da8e177a58951f0743cf9caa16886493c5538d03245c9b165592433f8a09c568dd7d9b605d0b4e1e78db0ac9920585cfe45156b2e367f3d37460dff9fc6eae4cff1cd708295c51
"""

//        let input = """
//5ad132ec4682d3f852887616b13ee19295292e084c00b917d0bb53a8bef644c65f171bf785bd4f4d0362be9b941c9c4fd452e5b84590b4f314aff10ac23ff7a1b0f6ce88abf882477dc8182a484149f5f5f54114bfd05f2807105a4067b99f3207c8f99330d5a7e0d4b7688d92f3f60952821e0f550a9c8e79973307735c978d2cb5786fa91a635c400797378da059e2c5059c36a5a0115e6a3b750b0c930927d083728e950ec0e02f4ae697eb3a1bbaa04aa91b8d8b1585debe1cb1dd7a3973
//"""

//        let input = """
//c15e8be60fc9910b7b2762b36f8e99246d2db4be924c1503e505a3a4327e090c3bb19046fdae8203c89df19708bd39a93512844666f2ab60195f3c127b73720acaa6ae2fc717a254be3571d9276d29184ea54e89305f67355c1056441fbc5b9b6e9d03d2e618141b43d93d25dc6e0dd0f634356a04909698b9d534c21340744791e5e088cc0623f6a928350273fc72a7ecb6e6380fc77aa284a571a7640bf61c402cdf3e392fe61390095338f72a7d46a4fb1f6aa8855f2943e48dc982887e3a6032f9ce0f487470f6449eb391065afcbc3d246f626b544e17f68e6b6e7c06a4acb49766054b079f829a1bbec314a43baf9a8b92cbe655d3e13224034117cbeb94b45a936411a83d549241e103f66eb1d60fe46e81f38d414a3201485eb2b6921cc503590361d0a781b7a5232b758544cc57f9c25064f8de6f977a2bfa61d812
//"""

//        let input = """
//c15e8be60fc9910b7b2762b36f8e99246d2db4be924c1503e505a3a4327e090c3bb19046fdae8203c89df19708bd39a9271cac7020a8f59ade4cf6a1464934b592ae094f0861535cde40bd318edcb58a3b1ca372576ac89531e62000b8e8c3e3f2103e844af1dec79656547947a4cf0e01414fcae9420a8836a921ee50e623cd7e76fd96267becd7be12d666ff2dd77e70be454ecebb9bc930e218f33c20a108e59c542ed2e594a520b7f477b992c614371f0f8890e45c202b1395ad28e7fd7a
//"""
        
//        let data = Data(base64Encoded: input, options: Data.Base64DecodingOptions(rawValue: 0))
//        print("input base64 decoded: \(String(describing: String(data: data!, encoding: .utf8)))")
        
        do {
            let aesDes: AES = try AES(key: key, blockMode: .ECB, padding: .pkcs7)
//            let r = try aesDes.decrypt(data!.bytes)
//            let r = try aesDes.decrypt(input.bytes)
            
//            let b = stringToBytes(input)
//            let r = try aesDes.decrypt(b!)
            
            let r = try aesDes.decrypt(Data(hex: input).bytes)
            
//            print(String(bytes: r, encoding: .utf8))
            let desData = Data(bytes: r)
//            print(desData)
            let s = String(data: desData, encoding: .utf8)
            print(s!)
        } catch {
            print(error)
        }
    
    }
    
//    func stringToBytes(_ string: String) -> [UInt8]? {
//        let length = string.characters.count
//        if length & 1 != 0 {
//            return nil
//        }
//        var bytes = [UInt8]()
//        bytes.reserveCapacity(length/2)
//        var index = string.startIndex
//        for _ in 0..<length/2 {
//            let nextIndex = string.index(index, offsetBy: 2)
//            if let b = UInt8(string[index..<nextIndex], radix: 16) {
//                bytes.append(b)
//            } else {
//                return nil
//            }
//            index = nextIndex
//        }
//        return bytes
//    }

}

//extension UnicodeScalar {
//    var hexNibble:UInt8 {
//        let value = self.value
//        if 48 <= value && value <= 57 {
//            return UInt8(value - 48)
//        }
//        else if 65 <= value && value <= 70 {
//            return UInt8(value - 55)
//        }
//        else if 97 <= value && value <= 102 {
//            return UInt8(value - 87)
//        }
//        fatalError("\(self) not a legal hex nibble")
//    }
//}
//
//
//extension Data {
//    init(hex:String) {
//        let scalars = hex.unicodeScalars
//        var bytes = Array<UInt8>(repeating: 0, count: (scalars.count + 1) >> 1)
//        for (index, scalar) in scalars.enumerated() {
//            var nibble = scalar.hexNibble
//            if index & 1 == 0 {
//                nibble <<= 4
//            }
//            bytes[index >> 1] |= nibble
//        }
//        self = Data(bytes: bytes)
//    }
//}

//
//  RouterStatus.swift
//  fp-ios
//
//  Created by 劉芳瑜 on 2018/5/23.
//  Copyright © 2018年 Fang-Yu. Liu. All rights reserved.
//

import Foundation


struct RouterStatus: Codable {
    
    var errorCode: Int
    var message: String
    var result: Result
    
    struct Result: Codable {
        var propSet: String
        var internetStatus: String
        var firmwareVersion: String
        var agentVersion: String
        var agentId: String
        var webfilterIp: String
        var webfilterStatus: String
        var cfgVersion: Int
    }
    
    static func decode(with data: Data) -> RouterStatus? {
        do {
            let decoder = JSONDecoder()

//RouterStatus(errorCode: 0, message: "Success", result: fp_ios.RouterStatus.Result(propSet: "wifiRtStatus", internetStatus: "false", firmwareVersion: "1.3.10", agentVersion: "1.3.10d", agentId: "ht+TtdnGDU/vZuAJaIKwxLF6Q6I=", webfilterIp: "159.89.193.228", webfilterStatus: "true", cfgVersion: 4835000))
//
            return try decoder.decode(RouterStatus.self, from: data)
            
        } catch (let error) {
            
            print("decode To RouterStatus failed", error.localizedDescription)
            return nil
        }
    }
}


//{"errorCode": 0, "message": "Success", "result":
//    {"propSet": "wifiRtStatus", "internetStatus": "false",
//        "firmwareVersion": "1.3.0", "agentVersion": "1.3.0",
//        "agentId": "ht+TtdnGDU/vZuAJaIKwxLF6Q6I=", "webfilterIp": "",
//        "webfilterStatus": "false", "cfgVersion": 1708110009}
//}


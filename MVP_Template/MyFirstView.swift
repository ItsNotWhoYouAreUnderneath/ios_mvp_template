//
//  MyFirstView.swift
//  MVP_Template
//
//  Created by Johnson Hsu on 2018/4/3.
//  Copyright © 2018年 Johnson Hsu. All rights reserved.
//

import Foundation

// MARD: Model
struct MyModel {
    var data1: String
    var data2: Int
}

struct MyModel2: Codable {
    var data1: String
    var data2: Int?
    var data3: Bool?
}


// MARK: View
protocol MyFirstView {
    
    func initPresenter()
    
    func action1()
    
    func action2()
    
}

protocol P {
    func doXxx1()
    func doXxx2()
}


// MARK: Presenter
public class MyFirstPresenter: P {
    
    var view: MyFirstView
    
    var model: [MyModel]?
    
    init(_ view: MyFirstView, model: [MyModel]?) {
        self.view = view
        if let m = model {
            self.model = m
        }
        
    }
    
    func doXxx1() {
        // call view interface, such as MyFirstView.action1()
    }
    
    func doXxx2() {
        
    }
    
    func processXxx1(_ jsonString: String) -> MyModel2? {
        do {
            let myModel2 = try JSONDecoder().decode(MyModel2.self, from: jsonString.data(using: .utf8)!)
            return myModel2
        } catch(let error) {
            print(error.localizedDescription)
            return nil
        }
    }
    
}

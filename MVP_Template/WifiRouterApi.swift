//
//  WifiRouterApi.swift
//  MVP_Template
//
//  Created by Johnson Hsu on 2018/5/27.
//  Copyright © 2018年 Johnson Hsu. All rights reserved.
//

import Foundation

/**
 To request to Wifi Router HTTP API.
 
 */
public class WifiRouterApi {
    
    static let url = URL(string: "http://192.168.0.1:9098")
    
    /**
     GetRouterStatus API
     */
    static func getRouterStatus(_ completion: @escaping (Data?, Error?) -> Void) {
        var request = URLRequest(url: URL(string: "/GetRouterStatus", relativeTo: url)!)
        request.httpMethod = "POST"
        request.setValue("text/plain", forHTTPHeaderField: "Content-Type")
        let task = URLSession.shared.dataTask(with: request) { (data, response, error) in
            if let error = error {
                print ("error: \(error)")
                completion(nil, error)
            }
            guard let response = response as? HTTPURLResponse else {
                //throw WifiRouterApiError("response objecct is not a HTTPURLResponse", "Response Error")
                return
            }
//            if response.statusCode != 200 {
//                completion(nil, WifiRouterApiError("status code: \(response.statusCode)", "Response status code is not 200"))
//            }
            switch response.statusCode {
            case 200:
                // success
                break
            case 401, 403:
                // TODO: 401 Unauthorized / 403 Forbidden => Authentication or Authorization failed, Try to auto login once
                break
            case 400, 405:
                // TODO: 400 Bad Request / 405 Method Not Allowed => API invocation errro, should update program
                break
            default:
                // TODO: other error
                break
            }
            
            
            if let data = data,
                let dataString = String(data: data, encoding: .utf8) {
                print ("got data: \(dataString)")
                completion(data, nil)
            }
        }
        task.resume()
    }
    
    /**
     SetRouterConfig API
     */
    static func setRouterConfig(_ data: String?, _ completion: @escaping (Data?, Error?) -> Void) {
        var request = URLRequest(url: URL(string: "/SetRouterConfig", relativeTo: url)!)
        request.httpMethod = "POST"
//        request.setValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
        request.setValue("text/html", forHTTPHeaderField: "Content-Type")
//        request.setValue("application/json", forHTTPHeaderField: "Content-Type")

        request.httpBody = Data(bytes: (data?.bytes)!)
        
        let task = URLSession.shared.dataTask(with: request) { (data, response, error) in
            if let error = error {
                print ("error: \(error)")
                completion(nil, error)
            }
            guard let response = response as? HTTPURLResponse else {
                //throw WifiRouterApiError("response objecct is not a HTTPURLResponse", "Response Error")
                return
            }
            if response.statusCode != 200 {
                completion(nil, WifiRouterApiError("status code: \(response.statusCode)", "Response status code is not 200"))
            }
            if let data = data,
                let dataString = String(data: data, encoding: .utf8) {
                print ("got data: \(dataString)")
                completion(data, nil)
            }
        }
        task.resume()
    }
    
}

struct WifiRouterApiError: LocalizedError {
    var failureReason: String?
    var errorDescription: String?
    
    init(_ failureReason: String?, _ errorDescription: String?) {
        self.failureReason = failureReason
        self.errorDescription = errorDescription
    }
}


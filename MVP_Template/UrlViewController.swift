//
//  UrlViewController.swift
//  MVP_Template
//
//  Created by Johnson Hsu on 2018/5/27.
//  Copyright © 2018年 Johnson Hsu. All rights reserved.
//

import UIKit

class UrlViewController: UIViewController {
    
//    var key = "GkoX2xxpoHyKSPDnAUd8vqxymvYv6M4TUe1MNxGPI5s="
//    var key = "aHQrVHRkbkdEVS92WnVBSmFJS3d4TEY2UTZJPYvUq4s="
    
    // meson
    var key = "aDBaN0djVXhBcVBTMWE2eGhkdUt1dHVoRWJvPc+lyLg="
    
    var cfgVersion: Int?


    override func viewDidLoad() {
        super.viewDidLoad()
//        doSend()
//        doSend2()
        
        call1()
    }
    
    func call1() {
        WifiRouterApi.getRouterStatus { (data, error) in
            if let error = error {
                print ("error: \(error)")
                return
            }
            if let data = data {
                let routerStatus = RouterStatus.decode(with: AESUtils.decrypt(input: String(data: data, encoding: .utf8)!, key: self.key).data(using: .ascii)!)
                self.cfgVersion = routerStatus?.result.cfgVersion
                self.call2()
                return
            }
        }
    }
    
    func call2() {
        let r = RouterConfig(wlansPassPhrase: "1111bbbb", cfgVersion: self.cfgVersion!)
        let encoder = JSONEncoder()
        let str = String(data: try! encoder.encode(r), encoding: .utf8)!
        let encStr = AESUtils.encrypt(input: str, key: key)
        
        WifiRouterApi.setRouterConfig(encStr) { (data, error) in
            if let error = error {
                print ("error: \(error)")
                return
            }
            if let data = data {
                let str = String(data: data, encoding: .utf8)!
                let decStr = AESUtils.decrypt(input: str, key: self.key)
                print("decrypted data:", decStr)
                return
            }
        }
    }

    func doSend() {
        let url = URL(string: "http://127.0.0.1:8080/")
        var request = URLRequest(url: url!)
        request.httpMethod = "GET"
        request.setValue("text/html", forHTTPHeaderField: "Content-Type")
        
//        let config = URLSessionConfiguration.default
//        let session = URLSession(configuration: config)

        URLSession.shared.dataTask(with: request) { (data, response, error) in
//        session.dataTask(with: request) { (data, response, error) in
            if let error = error {
                print ("error: \(error)")
                return
            }
            guard let response = response as? HTTPURLResponse,
                (200...299).contains(response.statusCode) else {
                    print ("server error")
                    return
            }
            if let mimeType = response.mimeType,
                mimeType == "text/html",
                let data = data,
                let dataString = String(data: data, encoding: .utf8) {
                print ("got data: \(dataString)")
            }
        }.resume()
    }
    
    func doSend2() {
        var request = URLRequest(url: URL(string: "http://127.0.0.1:8080/examples/servlets/servlet/RequestParamExample")!)
        request.httpMethod = "POST"
        request.setValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
        request.httpBody = "firstname=john".data(using: .ascii)
        
        URLSession.shared.dataTask(with: request) { (data, response, error) in
            if let error = error {
                print ("error: \(error)")
                return
            }
            guard let response = response as? HTTPURLResponse,
                (200...299).contains(response.statusCode) else {
                    print ("server error")
                    return
            }
            if let mimeType = response.mimeType,
                mimeType == "text/html",
                let data = data,
                let dataString = String(data: data, encoding: .utf8) {
                print ("got data: \(dataString)")
            }
        }.resume()
        
    }

}


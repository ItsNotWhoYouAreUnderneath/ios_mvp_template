//
//  ViewController.swift
//  MVP_Template
//
//  Created by Johnson Hsu on 2018/4/3.
//  Copyright © 2018年 Johnson Hsu. All rights reserved.
//

import UIKit

class ViewController: UIViewController, MyFirstView {
    
    var model: [MyModel] = [] {
        didSet {
            DispatchQueue.main.async {
                // Update View with changed model data
                
            }
        }
    }
    
    @IBOutlet weak var button1: UIButton!
    
    @IBOutlet weak var textfield1: UITextField!
    
    var presenter: P?
    
    func initPresenter() {
        presenter = MyFirstPresenter(self, model: model)
    }
    
    @IBAction func onclick(_ sender: Any) {
        textfield1.isEnabled = false
    }
    
    func action1() {
        print("action 1")
    }
    
    func action2() {
        print("action 2")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        initPresenter()
        textfield1.isEnabled = false
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}


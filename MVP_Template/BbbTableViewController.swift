//
//  BbbTableViewController.swift
//  MVP_Template
//
//  Created by Johnson Hsu on 2018/4/25.
//  Copyright © 2018年 Johnson Hsu. All rights reserved.
//

import UIKit

class BbbTableViewController: UITableViewController, BbbView {
    
    var presenter: BbbPresenter?
    
    var userProfile: UserProfile?
    
    var websiteCategories: [WebsiteCategory] = [
        WebsiteCategory(id: 1, name: "全年齡避免接觸", opened: false),
        WebsiteCategory(id: 2, name: "高中畢業前避免接觸", opened: false),
        WebsiteCategory(id: 3, name: "國中畢業前避免接觸", opened: false),
        WebsiteCategory(id: 4, name: "小學畢業前避免接觸", opened: false),
        WebsiteCategory(id: 5, name: "學齡前避免接觸", opened: false),
        WebsiteCategory(id: 6, name: "一般網站", opened: false)
    ]
    
    var selectedWebsiteCategoryDict = websiteCategoryDict
    
    var selectedAge: AgeClassification?
    
    func initPresenter() {
        presenter = BbbPresenter(self)
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
        initPresenter()
        if selectedAge?.id != 5 {
            selectDefaultWebsiteCategory()
        } else {
            selectCustomWebsiteCategory()
        }
        
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return websiteCategories.count
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        if websiteCategories[section].opened {
            switch section {
            case 0:
                return categoryDict[.CAT1]!.count + 1
            case 1:
                return categoryDict[.CAT2]!.count + 1
            case 2:
                return categoryDict[.CAT3]!.count + 1
            case 3:
                return categoryDict[.CAT4]!.count + 1
            case 4:
                return categoryDict[.CAT5]!.count + 1
            case 5:
                return categoryDict[.CAT6]!.count + 1
            default:
                return 1
            }
        } else {
            return 1
        }
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "BbbCell", for: indexPath) as! MyTableViewCell
        
        if indexPath.row == 0 {
            // this is a section title cell
//            cell.textLabel?.text = websiteCategories[indexPath.section].name
            cell.titleLabel?.text = websiteCategories[indexPath.section].name
            cell.subtitleLabel?.text = ""
            cell.accessoryType = .none
            cell.backgroundColor = #colorLiteral(red: 0.624061048, green: 0.8217533827, blue: 0.7587156892, alpha: 1)
        } else {
            // this is a cell inside a section rendering second level
            var selectedCategory: CategoryCode
            switch indexPath.section {
            case 0:
                selectedCategory = .CAT1
            case 1:
                selectedCategory = .CAT2
            case 2:
                selectedCategory = .CAT3
            case 3:
                selectedCategory = .CAT4
            case 4:
                selectedCategory = .CAT5
            case 5:
                selectedCategory = .CAT6
            default:
                selectedCategory = .CAT1
                break
            }
            
            let w = selectedWebsiteCategoryDict[categoryDict[selectedCategory]![indexPath.row - 1]]
            
            // reset cell for reuse
            cell.backgroundColor = UIColor.clear
//            cell.textLabel?.text = w!.name
            cell.titleLabel.text = w!.name
            cell.subtitleLabel.text = w!.desc
            
            cell.tag = w!.id
            if w!.checked {
                cell.accessoryType = .checkmark
            } else {
                cell.accessoryType = .none
            }
        }
        
        cell.setCallback { t in
            print("this is my callback")
            self.updateXxx(t)
        }
        return cell
    }
    
    func updateXxx(_ i: Int) {
        print(selectedAge)
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//        if websiteCategories[indexPath.section].opened {
//            websiteCategories[indexPath.section].opened = false
//        } else {
//            websiteCategories[indexPath.section].opened = true
//        }
        
        if indexPath.row == 0 {
            websiteCategories[indexPath.section].opened = !websiteCategories[indexPath.section].opened
            
            let sections = IndexSet.init(integer: indexPath.section)
            tableView.reloadSections(sections, with: .none)
        } else {
            let cell = tableView.cellForRow(at: indexPath)!
            
            if let (k, v) = findWebsiteCategoryItemInDict(cell.tag) {
                var w = v
                if w.checked {
                    w.checked = false
                    cell.accessoryType = .none
                } else {
                    w.checked = true
                    cell.accessoryType = .checkmark
                }
                selectedWebsiteCategoryDict.updateValue(w, forKey: k)
            }
            
            
//            for (k,v) in selectedWebsiteCategoryDict {
//                if v.id == cell.tag {
//                    var w = v
//                    if v.checked {
//                        w.checked = false
//                        cell.accessoryType = .none
//                    } else {
//                        w.checked = true
//                        cell.accessoryType = .checkmark
//                    }
//                    selectedWebsiteCategoryDict.updateValue(w, forKey: k)
//                    break
//                }
//            }
        }
    }
    
    /**
    Find WebsitecCategoryItem in selectedWebsiteCategoryDict with given WebsitecCategoryItem.id
    It should not be more than one result be the data structure, so that, always reture the first result.
 
    - parameters:
        - _ websiteCategoryItemId WebsitecCategoryItem.id
 
    - returns:
        The first match element in the Dict
     */
    func findWebsiteCategoryItemInDict(_ websiteCategoryItemId: Int) -> (UserProfile.WebsiteCategory, WebsiteCategoryItem)? {
        return selectedWebsiteCategoryDict.first { (k, v) -> Bool in
            return v.id == websiteCategoryItemId
        }
    }

    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    /**
     依選取的年齡分級，勾選 web site category.
     
     
    */
    func selectDefaultWebsiteCategory() {
        switch selectedAge?.id {
        case 1:
            selectCategory(.CAT1)
            selectCategory(.CAT2)
            selectCategory(.CAT3)
            selectCategory(.CAT4)
            selectCategory(.CAT5)
        case 2:
            selectCategory(.CAT1)
            selectCategory(.CAT2)
            selectCategory(.CAT3)
            selectCategory(.CAT4)
        case 3:
            selectCategory(.CAT1)
            selectCategory(.CAT2)
            selectCategory(.CAT3)
        case 4:
            selectCategory(.CAT1)
            selectCategory(.CAT2)
        case 5:
            selectCategory(.CAT1)
        default:
            break
        }
    }
    
    /**
     選取指定類別下的所有 web site category.
     
     - parameters:
        - category: 指定的分類
     
     */
    func selectCategory(_ category: CategoryCode) {
        categoryDict[category]!.forEach { (c) in
            selectedWebsiteCategoryDict[c]?.checked = true
        }
    }
    
    func selectCustomWebsiteCategory() {
        let blocks: [UserProfile.WebsiteCategory] = (userProfile?.blockedCategories)!
        
        for b in blocks {
            selectedWebsiteCategoryDict[b]?.checked = true
        }
    }
    
    /**
     依畫面勾選產生 web site category 清單
     
     - returns:
     陣列
     
     - seealso:
     
     */
    func getWebsiteCategory() -> [UserProfile.WebsiteCategory] {
        var b: [UserProfile.WebsiteCategory] = []
        selectedWebsiteCategoryDict.forEach { (k, v) in
            if v.checked {
                b.append(k)
            }
        }
        return b
    }
    
}

class MyTableViewCell: UITableViewCell {
    
    @IBOutlet weak var titleLabel: UILabel!
    
    @IBOutlet weak var subtitleLabel: UILabel!
    
    var presenter: BbbPresenter?
    
    var callback: ((Int) -> Void)?
    
    func setPresenter(presenter: BbbPresenter) {
        self.presenter = presenter
    }
    
    func setCallback(callback: @escaping (Int) -> Void) {
        self.callback = callback
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.layoutSubviews()
        print("layoutSubviews")
        NSLog("%0.4f", CGFloat.pi)
//        presenter!.updateWebsiteCategory()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
        callback!(self.tag)
    }
    
    @IBAction func action1(sender: Any) {
        
        callback!(self.tag)
    }
    
}


//
//  AESUtils.swift
//  fp-ios
//
//  Created by 劉芳瑜 on 2018/5/17.
//  Copyright © 2018年 Fang-Yu. Liu. All rights reserved.
//

import Foundation
import CryptoSwift

/**
 AES 256, ECB, Pkcs7 加密法
 */
class AESUtils {
    
    /**
     AES 256, ECB, Pkcs7 加密法
     - parameters:
        - input: 明文
        - key: AES Key
     - returns: 密文
     
     */
    static func encrypt(input: String, key: String) -> String {
        
        let key: Array<UInt8> = Data(base64Encoded: key, options: Data.Base64DecodingOptions(rawValue: 0))!.bytes
        
        let input = input
        do {
            let aesDes: AES = try AES(key: key, blockMode: .ECB, padding: .pkcs7)
            let r = try aesDes.encrypt(input.bytes)
            print(r.toHexString())
            return r.toHexString()
        } catch {
            print(error)
            return ""
        }
    }

    /**
     AES 256, ECB, Pkcs7 解密法
     - parameters:
     - input: 密文
     - key: AES Key
     - returns: 明文
     
     */
    static func decrypt(input: String, key: String) -> String {

        let skey: Array<UInt8> = Data(base64Encoded: key, options: Data.Base64DecodingOptions(rawValue: 0))!.bytes

        do {
            let aesDes: AES = try AES(key: skey, blockMode: .ECB, padding: .pkcs7)
      
            let r = try aesDes.decrypt(Data(hex: input).bytes)
            guard let str = String(bytes: r, encoding: .utf8) else {
                return "AES Decrypt Failed!"
            }
            print(str)
            return str
        
        } catch {
            print(error)
            return ""
        }
    }
}

extension UnicodeScalar {
    var hexNibble:UInt8 {
        let value = self.value
        if 48 <= value && value <= 57 {
            return UInt8(value - 48)
        }
        else if 65 <= value && value <= 70 {
            return UInt8(value - 55)
        }
        else if 97 <= value && value <= 102 {
            return UInt8(value - 87)
        }
        fatalError("\(self) not a legal hex nibble")
    }
}

extension Data {
    init(hex:String) {
        let scalars = hex.unicodeScalars
        var bytes = Array<UInt8>(repeating: 0, count: (scalars.count + 1) >> 1)
        for (index, scalar) in scalars.enumerated() {
            var nibble = scalar.hexNibble
            if index & 1 == 0 {
                nibble <<= 4
            }
            bytes[index >> 1] |= nibble
        }
        self = Data(bytes: bytes)
    }
}

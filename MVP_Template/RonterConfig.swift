//
//  RouterConfig.swift
//  fp-ios
//
//  Created by 劉芳瑜 on 2018/5/17.
//  Copyright © 2018年 Fang-Yu. Liu. All rights reserved.
//

import Foundation

struct RouterConfig: Codable {
    
    var lanIpaddr: String?
    var lanDhcp: Bool?
    var lanMacaddr: String?
    var lanSubnetmask: String?
    
    var wanSubnetmask: String?
    var wanPrimaryDns: String?
    var wanSecondaryDns: String?
    var wanUsername: String?
    var wanMacaddr: String?
    var wanPassword: String?
    var wanIpaddr: String?
    var wanMtu: Int?
    var wanType: String?
    var wanStatus: Bool?
    var wanGateway: String?
    
    var wlansEnabled: String?
    var wlansEncryption: String?
    var wlansPassPhrase: String?
    var wlansType: String?
    
    var wlanEnabled: Bool?
    var wlanSsidBroadcast: Bool?
    var wlanSsid: String?
    var wlanMacaddr: String?
    var wlanMode: String?
    
    var cfgVersion: Int
    
    static func decode(with data: Data) -> RouterConfig? {
        do {
            let decoder = JSONDecoder()
            
            return try decoder.decode(RouterConfig.self, from: data)
            
        } catch (let error) {
            print("decode To RouterConfig failed", error.localizedDescription)
            return nil
        }
    }
    
    // {"wlansPassPhrase":"1111qqqq", "wlansType":"WPA+2PSK", "wlansEnabled":"true", "wlansEncryption":"TKIP", "cfgVersion":1169005}
    init(wlansPassPhrase: String, cfgVersion: Int) {
        
        self.lanIpaddr = nil
        self.lanDhcp = nil
        self.lanMacaddr = nil
        self.lanSubnetmask = nil
        
        self.wanSubnetmask = nil
        self.wanPrimaryDns = nil
        self.wanSecondaryDns = nil
        self.wanUsername = nil
        self.wanMacaddr = nil
        self.wanPassword = nil
        self.wanIpaddr = nil
        self.wanMtu = nil
        self.wanType = nil
        self.wanStatus = nil
        self.wanGateway = nil
        
        self.wlansEnabled = "true"
        self.wlansEncryption = "TKIP"
        self.wlansPassPhrase = wlansPassPhrase
        self.wlansType = "WPA+2PSK"
        
        self.wlanEnabled = nil
        self.wlanSsidBroadcast = nil
        self.wlanSsid = nil
        self.wlanMacaddr = nil
        self.wlanMode = nil
        
        self.cfgVersion = cfgVersion
    }
}

struct RouterConfigApiResponse: Codable {
    var errorCode: Int
    var message: String
    var result: Result?
    
    struct Result: Codable {
        var wlansPassPhrase: String
        var wlansType: String
        var wlansEnabled: String
        var wlansEncryption: String
        var cfgVersion: Int
    }
    
    static func decode(with data: Data) -> RouterConfigApiResponse? {
        do {
            let decoder = JSONDecoder()
            
            return try decoder.decode(RouterConfigApiResponse.self, from: data)
            
        } catch(let error) {
            //error alert
            print("decode To RouterConfigApiResponse failed", error.localizedDescription)
            return nil
        }
    }
}




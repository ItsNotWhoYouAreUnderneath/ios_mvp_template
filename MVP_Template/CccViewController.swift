//
//  CccViewController.swift
//  MVP_Template
//
//  Created by Johnson Hsu on 2018/5/24.
//  Copyright © 2018年 Johnson Hsu. All rights reserved.
//

import UIKit

class CccViewController: UIViewController {
    
    var waitingTime = 300

    @IBOutlet weak var timerLabel: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        var tt = Timer.scheduledTimer(withTimeInterval: 1, repeats: true) { (t) in
            self.waitingTime -= 1
            self.timerLabel.text = "count down.. \(self.waitingTime)"
        }
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

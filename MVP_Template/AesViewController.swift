//
//  AesViewController.swift
//  MVP_Template
//
//  Created by Johnson Hsu on 2018/5/23.
//  Copyright © 2018年 Johnson Hsu. All rights reserved.
//

import UIKit
import CryptoSwift

class AesViewController: UIViewController {
    
    let defautlKey: Array<UInt8> = Data(base64Encoded: "GkoX2xxpoHyKSPDnAUd8vqxymvYv6M4TUe1MNxGPI5s=", options: Data.Base64DecodingOptions(rawValue: 0))!.bytes
    
    // MARK: View Behaviors
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    // MARK: Business Logics
    
    func encrypt(key: Array<UInt8>, text: String) throws -> Array<UInt8> {
        let aesDes: AES = try AES(key: key, blockMode: .ECB, padding: .pkcs7)
        return try aesDes.encrypt(text.bytes)
    }
    
    func decrypt(key: Array<UInt8>, text: String) throws -> Array<UInt8> {
        let aesDes: AES = try AES(key: key, blockMode: .ECB, padding: .pkcs7)
        return try aesDes.decrypt(Data(hex: text).bytes)
    }
    
    // MARK: IB Connections
    
    @IBOutlet weak var keyInBase64: UITextField!
    
    /** input/output clear text */
    @IBOutlet weak var decryptionText: UITextView!
    
    /** input/output encrypted text */
    @IBOutlet weak var encryptionText: UITextView!
    
    @IBAction func resetAction(_ sender: UIButton) {
        decryptionText.text = ""
        encryptionText.text = ""
    }
    
    @IBAction func encryptAction(_ sender: UIButton) {
        var key: Array<UInt8>
        if keyInBase64.text != nil && keyInBase64.text != "" {
            key = Data(base64Encoded: keyInBase64.text!, options: Data.Base64DecodingOptions(rawValue: 0))!.bytes
        } else {
            key = defautlKey
        }
        
        if let t = decryptionText.text {
            do {
                try encryptionText.text = self.encrypt(key: key, text: t).toHexString()
            } catch {
                encryptionText.text = "error"
            }
        }
    }
    
    @IBAction func decryptAction(_ sender: UIButton) {
        var key: Array<UInt8>
        if keyInBase64.text != nil && keyInBase64.text != "" {
            key = Data(base64Encoded: keyInBase64.text!, options: Data.Base64DecodingOptions(rawValue: 0))!.bytes
        } else {
            key = defautlKey
        }
        
        if let t = encryptionText.text {
            do {
                var r = try String(bytes: self.decrypt(key: key, text: t), encoding: .utf8)
                if r == nil {
                    r = "decrypt failed, could be wrong key!"
                }
                decryptionText.text = r
            } catch {
                decryptionText.text = "error"
            }
        }
    }
    
}


///**
// 
// */
//private extension UnicodeScalar {
//    var hexNibble:UInt8 {
//        let value = self.value
//        if 48 <= value && value <= 57 {
//            return UInt8(value - 48)
//        }
//        else if 65 <= value && value <= 70 {
//            return UInt8(value - 55)
//        }
//        else if 97 <= value && value <= 102 {
//            return UInt8(value - 87)
//        }
//        fatalError("\(self) not a legal hex nibble")
//    }
//}
//
//
///**
// 
// */
//private extension Data {
//    init(hex:String) {
//        let scalars = hex.unicodeScalars
//        var bytes = Array<UInt8>(repeating: 0, count: (scalars.count + 1) >> 1)
//        for (index, scalar) in scalars.enumerated() {
//            var nibble = scalar.hexNibble
//            if index & 1 == 0 {
//                nibble <<= 4
//            }
//            bytes[index >> 1] |= nibble
//        }
//        self = Data(bytes: bytes)
//    }
//}

//
//  Family.swift
//  fp-ios
//
//  Created by 劉芳瑜 on 2018/4/2.
//  Copyright © 2018年 Fang-Yu. Liu. All rights reserved.
//

import Foundation
import UIKit

struct UserProfile: Codable, Equatable {
    
    // For Unit Test
    static func ==(lhs: UserProfile, rhs: UserProfile) -> Bool {
        return
            // error handle nil
            
            lhs.id == rhs.id
    }
    
    enum WebsiteCategory: String, Codable {
        // 不可更改 enum 次序，資料庫紀錄的是 enum ordinal
        /**
         * Name: 未分類。系統內部內別，使用者無須見到此類別
         * Description: 網址分類服務提供者無法分類的網站。
         *      停止運作的網站：這些是死的網站，不回應http查詢
         *      殭屍網路：這些地址被確定為殭屍網絡的一部分，從中發起網絡攻擊。攻擊可能包括垃圾信息，
         *              阻斷服務，SQL注入和其他自動發出的聯繫。
         *      垃圾郵件：發送垃圾郵件的網址
         *      動態生成內容：根據傳入網站的參數或其他資訊參（如地理位置）動態生成內容的網站
         *      寄放網域：只含有限的內容，或可能產生收益的點擊廣告，但通常不包含對用戶有用的內容。
         *              還包括“正在構建”，文件夾和Web服務器默認主頁。
         *      Private IP Addresses。
         */
        case UNCATEGORIZED, //(0),
        /**
         * Name: 惡意網站。
         * Description: 惡意內容，包括由其他受感染站台驅動執行的軟體，惡意腳本，病毒，特洛伊木馬和程式碼。
         *      也包括網絡釣魚，網址欺詐以及冒充成信譽良好網站收集用戶個人信息。
         */
        MALICIOUS, //(1),
        /**
         * Name: 成人和色情。
         * Description:為了喚起性興趣或性騷擾的目的的色情內容與材料。 成人產品，包括性玩具，CD-ROM和視訊。
         *      成人服務，包括視訊會議，陪同服務和脫衣舞俱樂部。色情導向的線上新聞群組和論壇。
         *      色情故事和性行為的文字說明。色情藝術。
         */
        ADULT_CONTENT, //(2),
        /**
         * Name:性教育。
         * Description:關於生殖，性發育，安全性行為，性傳播疾病，性行為，生育控制，
         *      更好性行為的提示以及用於性生活的產品和避孕藥具等信息。
         */
        SEX_EDUCATION, //(3),
        /**
         * Name:裸露。
         * Description:人體的裸體或修飾描繪。 這些描繪不一定含有性意圖或效果，
         *      但可能包括含有裸體繪畫或藝術性質的照片畫廊。
         *      包括裸體主義者或裸體主義者的網站，其中包含裸體個人的照片。
         */
        NUDITY, //(4),
        /**
         * Name:泳裝和貼身服裝
         * Description:游泳衣（比基尼），貼身衣物或其他類型的建議服裝。
         */
        SWIMSUITS_AND_INTIMATE_APPAREL, //(5),
        /**
         * Name:賭博。
         * Description:用真實或虛擬的錢進行賭博或樂透的網站。 賭博的信息或建議。 虛擬賭場和境外賭博。
         *       體育選秀和博彩池。提供大量的獎勵或要求投注的虛擬體育和幻想聯賽。
         */
        GAMBLING, //(6),
        /**
         * Name:仇恨和種族主義
         * Description:包含支持仇恨犯罪和種族主義的內容和語言的網站，如納粹，新納粹，三K黨等。
         */
        HATE_AND_RACISM, //(7),
        /**
         * Name:暴力
         * Description:提倡暴力的網站，提供描述和方法，包括暴力遊戲/漫畫和自殺。
         */
        VIOLENCE, //(8),
        /**
         * Name:作弊。
         * Description:支持作弊並包含這些材料的網站，包括免費散文，考試拷貝，剽竊等。
         */
        CHEATING, //(9),
        /**
         * Name:非法。
         * Description:犯罪活動，如何不被抓到，版權和知識產權侵犯等。
         */
        ILLEGAL, //(10),
        /**
         * Name:墮胎。
         * Description:墮胎話題，無論是反對或支持墮胎。
         */
        ABORTION, //(11),
        /**
         * Name:酒精和煙草。
         * Description:提供信息，促進或支持銷售含酒精飲料或煙草製品及相關用具的網站。
         */
        ALCOHOL_AND_TOBACCO, //(12),
        /**
         * Name:大麻。
         * Description:大麻的使用，種植，歷史，文化，法律問題。
         */
        MARIJUANA, //(13),
        /**
         * Name:濫用藥物
         * Description:討論或補救非法或濫用藥物，如海洛因，可卡因或其他街頭毒品。
         *      關於“合法快感”的信息：吸食強力膠，濫用處方藥或濫用其他合法物質等。
         */
        ABUSED_DRUGS, //(14),
        /**
         * Name:駭客與破解。
         * Description:非法使用軟件/通訊設備。開發和散佈可能危及網絡和系統的程式。規避需要付費使用的電腦軟體或其他系統。
         */
        HACKING, //(15),
        /**
         * Name:武器。
         * Description:武器的銷售，評論或描述，如槍支，刀具或武術裝置，或提供有關其使用，配件或其他修改的信息。
         */
        WEAPONS, //(16),
        /**
         * Name:間諜與廣告軟件。
         * Description:間諜軟件或廣告軟件網站，未經使用者或組織明確同意就收集訊息或進行追蹤，
         *      也有可能未經請求就彈出廣告，或在使用者電腦安裝程式。
         */
        SPYWARE_AND_ADWARE, //(17),
        /**
         * Name:遊戲
         * Description:玩戲玩或下載遊戲，視訊遊戲，電腦遊戲，電子遊戲，提示，遊戲建議或如何獲取作弊碼。
         *      還包括致力於銷售棋盤遊戲的網站以及致力於玩遊戲的期刊和雜誌。
         *      包括支持或託管線上抽獎和贈品的網站。 包括舉辦遊戲的幻想運動網站。
         */
        GAMES, //(18),
        /**
         * Name: 可疑的網站
         * Description: 無味的幽默，“快速致富”網站，以及以某種不尋常的，意想不到的或可疑的方式操縱用戶瀏覽器的網站。
         */
        QUESTIONABLE, //(19),
        /**
         * Name:粗俗
         * Description:嘔吐物和其他身體機能、血腥的服裝等內容。
         */
        GROSS, //(20),
        /**
         * Name:匿名聯網
         * Description:使用代理伺服器或其他方法繞過網址過濾或監控，以匿名方式訪問網站。
         */
        ANONYMIZERS, //(21),
        /**
         * Name:崇拜和神祕
         * Description:通過使用占星術，法術，詛咒，魔力，撒旦或超自然的生命來解釋，影響或影響真實事件的方法，
         *      教導或其他資源。 包括星座網站。
         */
        CULT_AND_OCCULT, //(22),
        
        /**
         * Name:拍賣
         * Description: 支持個人之間提供和購買商品的網站
         */
        AUCTIONS, //(23),
        /**
         * Name:購物
         * Description:百貨公司，零售店，公司/產品目錄和其他型態網站，在這些網站允許線上消費或購買商品與服務。
         */
        SHOPPING, //(24),
        
        /**
         * Name: 電腦與互聯網
         * Description: 一般電腦和互聯網的技術資料網站，互聯網軟體服務的網站,第三方內容和數據傳遞網路，網頁託管服務。
         */
        COMPUTER_INTERNET, //(25),
        /**
         * Name:社交網路。
         * Description:這些社交網站具有用戶社區，用戶在其中進行互動，發布訊息、圖片和其他通信。
         */
        SOCIAL_NETWORK, //(26),
        /**
         * Name:約會交友。
         * Description:約會（交友）網站，專注於建立人際關係。
         */
        DATING, //(27),
        /**
         * Name:互聯網通信。
         * Description:互聯網電話，簡訊，IP網路電話服務和相關業務。
         */
        INTERNET_COMMUNICATIONS, //(28),
        /**
         * Name:搜索引擎。
         * Description:使用關鍵詞或短語搜索，返回的結果可能包括文本，網站，圖像，視頻和文件。
         */
        SEARCH_ENGINES, //(29),
        /**
         * Name:入口網站。
         * Description:聚合更廣泛的互聯網內容和主題的網站，通常作為用戶的首頁，或使用某類型服務的出發點。
         */
        INTERNET_PORTALS, //(30),
        /**
         * Name:電子郵件。
         * Description:提供基於網頁的電子郵件和電子郵件客戶端程式。
         */
        WEB_BASED_EMAIL, //(31),
        /**
         * Name:圖像和視訊搜索。
         * Description:照片和圖像搜索，線上相冊/數位照片交換，圖像託管。
         */
        IMAGE_AND_VIDEO_SEARCH, //(32),
        /**
         * Name:線上翻譯。
         * Description:網址和語言翻譯網站，允許用戶查看其他語言的網頁。
         */
        TRANSLATION, //(33),
        /**
         * Name: 網頁廣告。
         * Description:廣告相關的媒體，內容和橫幅。用現金或獎品支付用戶點擊或閱讀特定鏈接，電子郵件或網頁的網站。
         */
        WEB_ADVERTISEMENTS, //(34),
        /**
         * Name:線上賀卡
         * Description:線上賀卡網站
         */
        GREETING_CARDS, //(35),
        /**
         * Name:個人儲存
         * Description:在線儲存，可存取布文件，音樂，圖片和其他數據。
         */
        PERSONAL_STORAGE, //(36),
        /**
         * Name:個人與部落格。
         * Description:個人或團體發布的個人網站，以及部落格。
         */
        PERSONAL_SITES_AND_BLOGS, //(37),
        /**
         * Name:參考和研究。
         * Description:個人，專業或教育參考資料，包括線上字典，地圖，人口普查，年鑑，圖書館目錄，族譜和科學信息。
         */
        REFERENCE_AND_RESEARCH, //(38),
        /**
         * Name:共享或免費軟體
         * Description:軟體，屏幕保護程式，圖標，壁紙，工具程式，鈴聲。 包括請求捐贈的下載和開源項目。
         */
        SHAREWARE_AND_FREEWARE, //(39),
        /**
         * Name:鍵盤記錄和監視
         * Description:下載和討論用來跟踪用戶打字或監控他們網頁瀏覽習慣的軟體。
         */
        KEYLOGGERS_AND_MONITORING, //(40),
        /**
         * Name:P2P檔案分享。
         * Description:點對點（P2P）用戶端及存取。 包括 torrents、emuel、音樂下載程式。
         */
        PEER_TO_PEER, //(41),
        /**
         * Name:新聞媒體。
         * Description:目前所發生的事件或當代的問題。還包括廣播電台、雜誌、線上報紙，頭條新聞網站，通訊社服務，個性化新聞服務和天氣網站
         */
        NEWS_AND_MEDIA, //(42),
        /**
         * Name:金融服務。
         * Description: 銀行服務和其他類型的財務信息，如貸款，會計，精算師，銀行，抵押貸款和一般保險公司。
         */
        FINANCIAL_SERVICES, //(43),
        /**
         * Name:股票投資與工具。
         * Description:促進和便利證券交易，投資資產管理。還包括有關財務投資策略，報價和新聞的信息。
         */
        STOCK_ADVICE_AND_TOOLS, //(44),
        /**
         * Name: 商業和經濟
         * Description: 商業公司，企業網站，商業信息，經濟學，市場營銷，管理和創業。
         */
        BUSINESS_AND_ECONOMY, //(45),
        /**
         * Name:宗教。
         * Description:傳統或非傳統的宗教或準宗教主題，以及教堂，猶太教堂或其他教堂。
         */
        RELIGION, //(46),
        /**
         * Name:法律。
         * Description:法律網站，律師事務所，法律問題的討論和分析。
         */
        LEGAL, //(47),
        /**
         * Name:運動
         * Description:團隊或聯盟網站。國際，國內，大學，專業分數和賽程。 體育相關的線上雜誌或新聞。幻想體育和虛擬體育聯賽。
         */
        SPORTS, //(48),
        /**
         * Name:社會。
         * Description:與普通民眾有關的各種議題，團體和協會以及影響包括安全，兒童，社會和慈善團體在內的各種人群的廣泛問題。
         */
        SOCIETY, //(49),
        /**
         * Name:政府
         * Description:有關政府，政府機構和政府服務的信息，如稅收，公共和緊急服務。
         *      還包括討論或解釋各種政府實體法律的網站。 包括地方，縣，州和國家政府的網站。
         */
        GOVERNMENT, //(50),
        /**
         * Name:教育機構。
         * Description:學前，小學，中學，高中，學院，大學，職業學校等教育內容和信息，包括招生，學費和教學大綱。
         */
        EDUCATIONAL_INSTITUTIONS, //(51),
        /**
         * Name:訓練與工具。
         * Description:遠距教學、貿易學校、線上課程、職業培訓、軟體培訓、技能訓練。
         */
        TRAINING_AND_TOOLS, //(52),
        /**
         * Name:哲學和政治
         * Description:政治，哲學，討論，推廣一個特定的觀點或立場以達進一步的目標。
         */
        PHILOSOPHY_AND_POLITICAL, //(53),
        /**
         * Name:軍事。
         * Description:有關軍事部門，武裝部隊和軍事歷史的信息。
         */
        MILITARY, //(54),
        /**
         * Name:食品與餐飲
         * Description: 飲食場所; 餐館，酒吧，小酒館和酒吧; 餐廳指南和評論。一般食物;
         *      食品與飲品; 烹飪和食譜; 食物和營養，健康和節食; 烹飪，包括食譜和烹飪網站。
         */
        FOOD_AND_DINING, //(55),
        /**
         * Name:兒童。
         * Description:專門為兒童和青少年設計的網站。
         */
        KIDS, //(56),
        /**
         * Name:娛樂和藝術。
         * Description:電影，視訊，電視，音樂和節目指南，書籍，漫畫，電影院，畫廊，藝術家或娛樂評論。
         *       表演藝術（戲劇，雜耍，歌劇，交響樂等）。博物館，畫廊，藝術家的網站（雕塑，攝影等）。
         */
        ENTERTAINMENT_AND_ARTS, //(57),
        /**
         * Name:串流媒體。
         * Description:音訊或視訊內容的銷售、交付、串流傳輸，包括為這些觀眾提供下載服務的網站。
         */
        STREAMING_MEDIA, //(58),
        /**
         * Name:音樂。
         * Description:音樂銷售、發行、串流，音樂團體和表演的信息，歌詞和音樂業務
         */
        MUSIC, //(59),
        /**
         * Name:健康和醫學。
         * Description:一般健康，健身，幸福，包括傳統和非傳統的方法和主題。
         *      有關疾病，牙科，精神病學，驗光和其他專業的醫學信息。醫院和醫生辦公室。 醫療保險。 整容手術。
         */
        HEALTH_AND_MEDICINE, //(60),
        /**
         * Name:家與庭院。
         * Description: 家庭問題和產品，包括維護，家居安全，裝飾，烹飪，園藝，家用電器，設計等。
         */
        HOME_AND_GARDEN, //(61),
        /**
         * Name:旅遊
         * Description:航空公司和航班預訂機構。 旅行計劃，預訂，車輛出租，旅遊目的地的描述，或旅館的促銷活動。 汽車出租。
         */
        TRAVEL, //(62),
        /**
         * Name:地方訊息。
         * Description:城市指南和旅遊資訊，包括餐館，地區資訊和當地旅遊景點。
         */
        LOCAL_INFORMATION, //(63),
        /**
         * Name:機動車輛
         * Description:汽車評論，車輛購買或銷售提示，零件目錄。 汽車交易，照片，
         *      包括摩托車，船隻，汽車，卡車和房車的討論。 車輛修改期刊和雜誌。
         */
        MOTOR_VEHICLES, //(64),
        /**
         * Name:求職。
         * Description:協助尋找工作，尋找未來雇主的工具。雇主尋找僱員的工具。
         */
        JOB_SEARCH, //(65),
        /**
         * Name: 房地產。
         * Description: 有關租賃，購買或出售房地產或房產的信息。 購買或出售房屋的提示。
         * 房地產經紀人，出租或搬遷服務，以及物業改善。
         */
        REAL_ESTATE, //(66),
        /**
         * Name:時尚與美麗。
         * Description:時尚或魅力雜誌，美容，衣服，化妝品，風格。
         */
        FASHION_AND_BEAUTY, //(67),
        /**
         * Name:休閒與愛好
         * Description:休閒消遣的信息，協會，論壇和出版物，如收集，成套飛機，徒步旅行，
         *          露營，攀岩，特定藝術，工藝或技術等戶外活動; 動物和寵物相關的信息，
         *          包括特定品種，訓練，展示和人道社會。
         */
        RECREATION_AND_HOBBIES, //(68),
        /**
         * Name:狩獵和釣魚
         * Description: 運動狩獵，槍支俱樂部和釣魚
         */
        HUNTING_AND_FISHING //(69)
    }
    
    enum AvatarType: String, Codable {
        case OLD_MAN_1,
        OLD_WOMAN_1,
        MAN_1,
        MAN_2,
        WOMAN_1,
        WOMAN_2,
        BOY_1,
        BOY_2,
        GIRL_1,
        GIRL_2,
        DEFAULT
    }
    
    enum Gender: String, Codable {
        case MALE
        case FEMALE
    }
    
    var id: Int?
    var name: String
    var manager: Bool
    var blockUnknownCategory: Bool
    var birthday: Date?
    var gender: Gender?
    var avatarType: AvatarType?
    var timeScheduleEnabled: Bool
    var contentFilterEnabled: Bool
    var blockedCategories: [WebsiteCategory]?
    var version: Int
    
    init() {
        self.id = nil
        self.name = ""
        self.manager = false
        self.blockUnknownCategory = false
        self.gender = .MALE
        self.avatarType = .DEFAULT
        self.timeScheduleEnabled = false
        self.contentFilterEnabled = false
        self.blockedCategories = [.MALICIOUS]
        self.version = 0
    }
    
    init(id: Int?, name: String, manager: Bool, blockUnknownCategory: Bool, birthday: Date, gender: Gender, avatarType: AvatarType, timeScheduleEnabled: Bool, contentFilterEnabled: Bool, blockedCategories: [WebsiteCategory], version: Int) {
        self.id = id
        self.name = name
        self.manager = manager
        self.blockUnknownCategory = blockUnknownCategory
        self.birthday = birthday
        self.gender = gender
        self.avatarType = avatarType
        self.timeScheduleEnabled = timeScheduleEnabled
        self.contentFilterEnabled = contentFilterEnabled
        self.blockedCategories = blockedCategories
        self.version = version
    }
}


//{
//    "resultType": "SUCCESS",
//    "data": [
//    {
//    "id": 4,
//    "name": "wordpress-rd",
//    "manager": true,
//    "blockUnknownCategory": false,
//    "avatarType": "MAN_1",
//    "timeScheduleEnabled": false,
//    "contentFilterEnabled": true,
//    "blockedCategories": [
//    "MALICIOUS"
//    ],
//    "wpUserId": 4,
//    "version": 0
//    },
//    {
//    "id": 6,
//    "name": "Bear",
//    "manager": false,
//    "blockUnknownCategory": false,
//    "birthday": 1161100800000,
//    "gender": "FEMALE",
//    "avatarType": "WOMAN_1",
//    "timeScheduleEnabled": true,
//    "contentFilterEnabled": true,
//    "blockedCategories": [
//    "MALICIOUS"
//    ],
//    "version": 1
//    },
//    {
//    "id": 8,
//    "name": "BeaRWa123",
//    "manager": false,
//    "blockUnknownCategory": false,
//    "birthday": 1161100800000,
//    "gender": "FEMALE",
//    "avatarType": "WOMAN_1",
//    "timeScheduleEnabled": false,
//    "contentFilterEnabled": true,
//    "blockedCategories": [
//    "MALICIOUS"
//    ],
//    "version": 0
//    },
//    {
//    "id": 10,
//    "name": "Test",
//    "manager": false,
//    "blockUnknownCategory": false,
//    "birthday": 1161100800000,
//    "gender": "FEMALE",
//    "avatarType": "WOMAN_1",
//    "timeScheduleEnabled": false,
//    "contentFilterEnabled": true,
//    "blockedCategories": [
//    "MALICIOUS"
//    ],
//    "version": 0
//    }
//    ]
//}












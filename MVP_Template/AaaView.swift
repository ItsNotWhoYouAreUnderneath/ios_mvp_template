//
//  AaaView.swift
//  MVP_Template
//
//  Created by Johnson Hsu on 2018/5/1.
//  Copyright © 2018年 Johnson Hsu. All rights reserved.
//

import Foundation

// MARK: View
protocol AaaView {
    
    func initPresenter()
    
}


// MARK: Presenter
public class AaaPresenter {
    
    var view: AaaView
    
    init(_ view: AaaView) {
        self.view = view
        loadUserProfile()
    }
    
    func loadUserProfile() {
        // Use MOYA to get UserProfile through REST API
        
    }
    
}


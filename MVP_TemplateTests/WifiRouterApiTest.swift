//
//  WifiRouterApiTest.swift
//  MVP_TemplateTests
//
//  Created by Johnson Hsu on 2018/5/30.
//  Copyright © 2018年 Johnson Hsu. All rights reserved.
//

import XCTest
@testable import MVP_Template

/**
 必須先設定電腦網路連到 Family+ WiFi Router 的熱點, if the router was Activated, should change AES key.
 */
class WifiRouterApiTest: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testGetRouterStatus() {
        // TODO: 1. prepare environment, data, etc.
        // TODO: 2. invoke logic to be tested
        // TODO: 3. check results
//        let key = "GkoX2xxpoHyKSPDnAUd8vqxymvYv6M4TUe1MNxGPI5s="
        let key = "aDBaN0djVXhBcVBTMWE2eGhkdUt1dHVoRWJvPc+lyLg="
        
        WifiRouterApi.getRouterStatus { (data, error) in
            if let error = error {
                XCTFail("getRouterStatus response error \(error)")
            }
            if let data = data {
                let decStr = AESUtils.decrypt(input: String(data:data, encoding: .utf8)!, key: key)
                XCTAssertNotNil(decStr, "decryption failed, might be wrong key")
                let r = RouterStatus.decode(with: decStr.data(using: .utf8)!)
                XCTAssertNotNil(r, "RouterStatus json: \(decStr) decode return nil")
            }
        }
        
        sleep(3)
    }
    
    /**
     先呼叫 getRouterStatus 取得 cfgVersion, 須在3秒內完成
     */
    func testSetRouterConfig() {
//        let key = "GkoX2xxpoHyKSPDnAUd8vqxymvYv6M4TUe1MNxGPI5s="
//        let key = "aHQrVHRkbkdEVS92WnVBSmFJS3d4TEY2UTZJPYvUq4s="
        let key = "aDBaN0djVXhBcVBTMWE2eGhkdUt1dHVoRWJvPc+lyLg="
        
        var routerStatus: RouterStatus?
        WifiRouterApi.getRouterStatus { (data, error) in
            if let error = error {
                XCTAssertNil(error, "getRouterStatus response error: \(error.localizedDescription)")
                XCTFail("getRouterStatus response error \(error)")
            }
            if let data = data {
                let decStr = AESUtils.decrypt(input: String(data:data, encoding: .utf8)!, key: key)
                XCTAssertNotEqual(decStr, "AES Decrypt Failed!", "decryption failed, might be wrong key")
                routerStatus = RouterStatus.decode(with: decStr.data(using: .utf8)!)
                XCTAssertNotNil(routerStatus, "RouterStatus json: \(decStr) decode return nil")
            }
        }
        
        sleep(3)
        
        let routerConfig = RouterConfig(wlansPassPhrase: "1111hhhh", cfgVersion: routerStatus!.result.cfgVersion)
        var encStr: String?
        do {
            let json = try JSONEncoder().encode(routerConfig)
            encStr = AESUtils.encrypt(input: String(data: json, encoding: .utf8)!, key: key)
        } catch {
            XCTFail("RouterConfig \(routerConfig) encode to json failed: \(error)")
        }
        
        WifiRouterApi.setRouterConfig(encStr!) { (data, error) in
            if let error = error {
                XCTFail("setRouterConfig response error \(error)")
            }
            if let data = data {
                let decStr = AESUtils.decrypt(input: String(data:data, encoding: .utf8)!, key: key)
                XCTAssertNotNil(decStr, "decryption failed, might be wrong key")
                let r = RouterConfigApiResponse.decode(with: decStr.data(using: .utf8)!)
                XCTAssertNotNil(r, "Response json: \(decStr) decode return nil")
                XCTAssertEqual(0, r?.errorCode)
            }
        }
        
        sleep(3)
    }
    
}

//
//  BbbTableViewControllerTest.swift
//  MVP_TemplateTests
//
//  Created by Johnson Hsu on 2018/5/2.
//  Copyright © 2018年 Johnson Hsu. All rights reserved.
//

import XCTest
@testable import MVP_Template

class BbbTableViewControllerTest: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testExample() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }
    
    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }
    
    func testSelectCustomWebsiteCategory() {
        let view = BbbTableViewController()
        var u = UserProfile()
        
        u.blockedCategories = [
            .GAMBLING, .HATE_AND_RACISM
        ]
        view.userProfile = u
        view.selectCustomWebsiteCategory()
        XCTAssertTrue(view.selectedWebsiteCategoryDict[.GAMBLING]!.checked)
        XCTAssertTrue(view.selectedWebsiteCategoryDict[.HATE_AND_RACISM]!.checked)
        XCTAssertFalse(view.selectedWebsiteCategoryDict[.VIOLENCE]!.checked)
    }
    
}

//
//  AaaTableViewControllerTest.swift
//  MVP_TemplateTests
//
//  Created by Johnson Hsu on 2018/5/1.
//  Copyright © 2018年 Johnson Hsu. All rights reserved.
//

import XCTest
@testable import MVP_Template

class AaaTableViewControllerTest: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testExample() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }
    
    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }
    
    func testSelectAge() {
        let view = AaaTableViewController()
        var u = UserProfile()
        
        u.blockedCategories = [
            .GAMBLING, .HATE_AND_RACISM, .VIOLENCE, .MALICIOUS, .SPYWARE_AND_ADWARE, .ILLEGAL
        ]
        view.userProfile = u
        view.selectAgeFromUserProfile()
        XCTAssertEqual(view.selectedAge!.id, 5)
        
        u.blockedCategories = [
            .GAMBLING, .HATE_AND_RACISM, .VIOLENCE, .MALICIOUS, .SPYWARE_AND_ADWARE
        ]
        view.userProfile = u
        view.selectAgeFromUserProfile()
        XCTAssertEqual(view.selectedAge!.id, 6)
        
        u.blockedCategories = [
            .HATE_AND_RACISM, .ILLEGAL, .GAMBLING, .MALICIOUS, .VIOLENCE, .SPYWARE_AND_ADWARE
        ]
        view.userProfile = u
        view.selectAgeFromUserProfile()
        XCTAssertEqual(view.selectedAge!.id, 5)
        
        u.blockedCategories = [
            .GAMBLING, .HATE_AND_RACISM, .VIOLENCE, .MALICIOUS, .SPYWARE_AND_ADWARE, .ILLEGAL
            ,.ABUSED_DRUGS, .ADULT_CONTENT
        ]
        view.userProfile = u
        view.selectAgeFromUserProfile()
        XCTAssertEqual(view.selectedAge!.id, 6)
        
        u.blockedCategories = [
            .GAMBLING, .HATE_AND_RACISM, .VIOLENCE, .MALICIOUS, .SPYWARE_AND_ADWARE, .ILLEGAL
            , .ABUSED_DRUGS, .ADULT_CONTENT, .DATING, .MARIJUANA, .HACKING, .WEAPONS, .QUESTIONABLE, .KEYLOGGERS_AND_MONITORING, .CHEATING, .GROSS, .ANONYMIZERS, .NUDITY, .ABORTION, .ALCOHOL_AND_TOBACCO
        ]
        view.userProfile = u
        view.selectAgeFromUserProfile()
        XCTAssertEqual(view.selectedAge!.id, 4)
        
        u.blockedCategories = [
            .GAMBLING, .HATE_AND_RACISM, .VIOLENCE, .MALICIOUS, .SPYWARE_AND_ADWARE, .ILLEGAL
            , .ABUSED_DRUGS, .ADULT_CONTENT, .DATING, .MARIJUANA, .HACKING, .WEAPONS, .QUESTIONABLE, .KEYLOGGERS_AND_MONITORING, .CHEATING, .GROSS, .ANONYMIZERS, .NUDITY, .ABORTION
        ]
        view.userProfile = u
        view.selectAgeFromUserProfile()
        XCTAssertEqual(view.selectedAge!.id, 6)
        
        u.blockedCategories = [
            .GAMBLING, .HATE_AND_RACISM, .VIOLENCE, .MALICIOUS, .SPYWARE_AND_ADWARE, .ILLEGAL
            , .ABUSED_DRUGS, .ADULT_CONTENT, .DATING, .MARIJUANA, .HACKING, .WEAPONS, .QUESTIONABLE, .KEYLOGGERS_AND_MONITORING, .CHEATING, .GROSS, .ANONYMIZERS, .NUDITY, .ABORTION, .ALCOHOL_AND_TOBACCO
            , .CULT_AND_OCCULT, .STOCK_ADVICE_AND_TOOLS, .SWIMSUITS_AND_INTIMATE_APPAREL, .PEER_TO_PEER
        ]
        view.userProfile = u
        view.selectAgeFromUserProfile()
        XCTAssertEqual(view.selectedAge!.id, 3)
        
        u.blockedCategories = [
            .GAMBLING, .HATE_AND_RACISM, .VIOLENCE, .MALICIOUS, .SPYWARE_AND_ADWARE, .ILLEGAL
            , .ABUSED_DRUGS, .ADULT_CONTENT, .DATING, .MARIJUANA, .HACKING, .WEAPONS, .QUESTIONABLE, .KEYLOGGERS_AND_MONITORING, .CHEATING, .GROSS, .ANONYMIZERS, .NUDITY, .ABORTION, .ALCOHOL_AND_TOBACCO
            , .STOCK_ADVICE_AND_TOOLS, .SWIMSUITS_AND_INTIMATE_APPAREL, .PEER_TO_PEER
        ]
        view.userProfile = u
        view.selectAgeFromUserProfile()
        XCTAssertEqual(view.selectedAge!.id, 6)
        
        u.blockedCategories = [
            .GAMBLING, .HATE_AND_RACISM, .VIOLENCE, .MALICIOUS, .SPYWARE_AND_ADWARE, .ILLEGAL
            , .ABUSED_DRUGS, .ADULT_CONTENT, .DATING, .MARIJUANA, .HACKING, .WEAPONS, .QUESTIONABLE, .KEYLOGGERS_AND_MONITORING, .CHEATING, .GROSS, .ANONYMIZERS, .NUDITY, .ABORTION, .ALCOHOL_AND_TOBACCO
            , .CULT_AND_OCCULT, .STOCK_ADVICE_AND_TOOLS, .SWIMSUITS_AND_INTIMATE_APPAREL, .PEER_TO_PEER
            , .AUCTIONS, .SEX_EDUCATION, .HUNTING_AND_FISHING
        ]
        view.userProfile = u
        view.selectAgeFromUserProfile()
        XCTAssertEqual(view.selectedAge!.id, 2)
        
        u.blockedCategories = [
            .GAMBLING, .HATE_AND_RACISM, .VIOLENCE, .MALICIOUS, .SPYWARE_AND_ADWARE, .ILLEGAL
            , .ABUSED_DRUGS, .ADULT_CONTENT, .DATING, .MARIJUANA, .HACKING, .WEAPONS, .QUESTIONABLE, .KEYLOGGERS_AND_MONITORING, .CHEATING, .GROSS, .ANONYMIZERS, .NUDITY, .ABORTION, .ALCOHOL_AND_TOBACCO
            , .CULT_AND_OCCULT, .STOCK_ADVICE_AND_TOOLS, .SWIMSUITS_AND_INTIMATE_APPAREL, .PEER_TO_PEER
            , .AUCTIONS, .SEX_EDUCATION, .HUNTING_AND_FISHING
            , .MILITARY, .SOCIAL_NETWORK, .GAMES, .PHILOSOPHY_AND_POLITICAL, .INTERNET_COMMUNICATIONS
        ]
        view.userProfile = u
        view.selectAgeFromUserProfile()
        XCTAssertEqual(view.selectedAge!.id, 1)
        
        u.blockedCategories = [
            .GAMBLING, .HATE_AND_RACISM, .VIOLENCE, .MALICIOUS, .SPYWARE_AND_ADWARE, .ILLEGAL
            , .REAL_ESTATE
        ]
        view.userProfile = u
        view.selectAgeFromUserProfile()
        XCTAssertEqual(view.selectedAge!.id, 6)
        
        u.blockedCategories = [
            .REAL_ESTATE
        ]
        view.userProfile = u
        view.selectAgeFromUserProfile()
        XCTAssertEqual(view.selectedAge!.id, 6)
        
    }
    
}

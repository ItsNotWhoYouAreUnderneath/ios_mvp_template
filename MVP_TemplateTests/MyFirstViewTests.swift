//
//  MyFirstView.swift
//  MVP_TemplateTests
//
//  Created by Johnson Hsu on 2018/4/6.
//  Copyright © 2018年 Johnson Hsu. All rights reserved.
//

import XCTest
@testable import MVP_Template

class MyFirstViewTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    // MARK: Test processXxx1()
    func testProcessXxx1() {
        let view = MockView()
        let presenter = MyFirstPresenter(view, model: nil)
        var str = """
        {"data1":"value 1","data2":10,"data3":true}
        """
        var m = presenter.processXxx1(str)
        print(m ?? "MyModel2 is nil")
        XCTAssertEqual("value 1", m?.data1)
        XCTAssertEqual(10, m?.data2)
        XCTAssertEqual(true, m?.data3)
        
        str = """
        {"data1":"value 2"}
        """
        m = presenter.processXxx1(str)
        print(m ?? "MyModel2 is nil")
        XCTAssertEqual("value 2", m?.data1)
        XCTAssertNil(m?.data2)
        XCTAssertNil(m?.data3)
    }
    
    // MARK: Mock of MyFirstView
    class MockView : MyFirstView {
        func initPresenter() {}
        func action1() {}
        func action2() {}
    }
    
}
